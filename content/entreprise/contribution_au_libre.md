+++
date = "2019-04-24T18:00:00+02:00"
title = "Une semaine de contribution aux logiciels libres"
author = "Tristram"
+++

## Comment contribuer à un bien commun

Cette question nous travaille depuis nos débuts.

Pierre en avait parlé dans un [article précédent](https://www.codeursenliberte.fr/entreprise/comment-contribuer-aux-communs/) suite à sa contribution à [Mattermost](https://mattermost.com/) mais il avait aussi esquissé l’idée qui a abouti à cette [semaine de contribution au libre](https://gitlab.com/CodeursEnLiberte/fondations/issues/60).

Cette idée est née du constat que même en ayant un [pot à cookies](https://gitlab.com/CodeursEnLiberte/fondations/wikis/Fonctionnement/Utilisation-du-Pot-à-Cookies), il nous est tout de même difficile de contribuer significativement à des projets libres, souvent parce que les projets du quotidien (professionnels ou personnels) prennent le pas sur ces contributions, ou parce qu'il est difficile de trouver la motivation et l'énergie individuellement.

Nous avons donc réfléchi à organiser une semaine de contribution au libre qui serait à la fois un projet collectif, mais également un moment de coupure dans notre agenda en réservant une semaine entière.

## Comment réellement contribuer

Dans les faits, la plupart des projets libres sont portés par des entreprises dont c’est le cœur d’activité. Le cas du _hacker_ qui contribue la nuit et le week-end ne nous semble pas généralisable, ni conciliable avec nos vies personnelles, ni souhaitable pour éviter l’isolement des contributeurs.

Par ailleurs, même si nombreuses entreprises publient leur code source, il s’agit souvent des projets liés à l’entreprise, pas vraiment utiles à des tiers. Difficile d’appeler ces projets des _communs_.

Et pourtant, des outils déjà utilisés par un grand nombre de personnes auraient besoin d’un peu plus d’amour, en particulier tous les outils qui aident à nous [dégoogliser](https://degooglisons-internet.org/fr/). Notre but était de contribuer à ces projets-là.

## Le coup de pied nécessaire

Notre _pot à cookies_ n’a pas suffi à nous motiver à sortir la tête du travail quotidien. Pour nous forcer la main, nous avons invité d’autres développeurs à participer et à coder avec nous.

Nous avons choisi des projets qui ont à la fois une visée politique qui nous parait importante (par exemple en soutenant l'excellent travail de [Framasoft](https://framasoft.org/)), mais également des projets de taille raisonnable sur lesquels une semaine de travail aurait un impact significatif :

* [Pytition](https://github.com/pytition/Pytition) - un outil de pétition en ligne en cours de développement
* [Mobilizon](https://joinmobilizon.org/fr/) - un outil pour gérer des communautés et des évènements
* [Framadate](https://framadate.org/) - un outil de vote
* [Etherpad-lite](https://etherpad.org/), aussi connu comme [framapad](https://framapad.org/)

Très tôt, nous avons regardé les projets similaires comme les [contrib'ateliers](https://contribateliers.frama.site/) et nous avons préféré ne pas faire un évènemement ouvert au grand public, car nous n'étions pas sur de pouvoir gérer un grand nombre de personnes ou des personnes novices en développement. Nous avons donc décidé de communiquer sur cet événement autour des nous dans les cercles de développeur·se·s dont nous faisons partie, et avons également invité les développeur·se·s et designer·euse·s impliqué·e·s sur les projets que nous voulons soutenir à nous rejoindre.

## Bilan de la semaine

Nous avons passé la semaine à [la maison du libre et des communs](https://www.facebook.com/MLCParis/) qui nous a prêté une grande salle, merci à elle ! Codeurs en Liberté a payé les repas du midi.


Nous étions entre dix et quinze selon les jours, et nous avons avancé différemment selon les projets :

* [Etherpad-lite](https://etherpad.org/) : pas mal de notre frustration avec etherpad vient de son interface, ce qui peut se corriger avec une simple évolution du CSS. Nous avons trouvé un nouveau thème très bien fait par le groupe [Colibri Outils Libres](https://pad.colibris-outilslibres.org/), qui a été proposé à etherpad et devrait être ajouté dans la prochaine release. :) Moins de travail que prévu donc dans ce projet, nous en avons profité pour corriger quelques bugs.
* [Pytition](https://github.com/pytition/Pytition) : Nous avons largement refondu l'interface graphique et implémenté plusieurs nouvelles fonctionnalités. Le projet est désormais proche de pouvoir publier une v2 !
* [Mobilizon](https://joinmobilizon.org/fr/) : Il a été un peu compliqué de contribuer à Mobilizon, en partie parce que le projet est encore en cours de design, difficile d'avancer sur des fonctionnalités tant que celles-ci n'ont pas été clairement définies, mais nous comptons bien pouvoir y consacrer du temps lors d’une prochaine occasion.
* [Framadate](https://framadate.org/) : Du travail de fond a été ralisé sur le projet, notamment en réécrivant la partie internationalisation, mais aussi en permettant de le rendre déployable facilement via Docker.


### Sentiments

* C’était chouette de travailler à plusieurs
* Nous avons pu travailler avec des camarades, alors que ce n’est pas le cas au quotidien
* Nous avons rencontré plein de personnes très chouettes
* Certains projets ont gagné d’autres contributeurs et ne sont plus tenus par une seule personne
* Finalement on peut faire beaucoup en peu de temps
* C’est très satisfaisant de corriger des bugs qui nous dérangent au quotidien

### Enseignements

* Une semaine est une bonne durée
    * On a le temps de rentrer dans le projet
    * On se connait un peu plus humainement
    * Ça nous pénalise pas trop professionnellement
    * On évite l’effet de lassitude
* Avoir les soirs libres pour notre vie familiale, associative, amicale, est un gros avantage
* La présence de core-developpers change tout, ne serait-ce que pour comprendre les enjeux du projet et la direction envisagée
* Avoir des tickets triés à l’avance, une analyse de l’ergonomie permettent de rentrer très rapidement dans le bain
* Une douzaine de personnes semble une limite au delà de laquelle il devient difficile de travailler ensemble facilement

## Et la suite ?

Nous avons passé un excellent moment, des outils largement utilisés sont un peu meilleurs et nous allons très probablement réitérer l’expérience.

Cela dit, nous n’avons pas la vocation de devenir des animateurs de ce genre d’évènements. Notre envie est que le format essaime, et que d’autres entreprises _libèrent_ leurs développeurs, graphistes, designers, une ou deux semaines par an. C’est en fait assez facile à mettre en place : si vous voulez faire votre propre semaine de contribution, contactez-nous, on vous expliquera plus en détail comment on s'est organisés.
