+++
date = "2021-09-01T13:12:00+01:00"
title = "Quand travailler et pour qui ?"
author = "Tristram"
+++

Lorsque nous nous [présentons](a-propos/qui), nous revendiquons le droit de garder individuellement « _la liberté de notre temps de travail et de nos missions_ ».

Comment est-ce que nous arrivons à concilier nos désirs individuels sans compromettre notre collectif ? C’est-à-dire ne pas siphoner la trésorie, éviter les tensions, tout en respectant le droit du travail.

L’actualité parle à nouveau des 32 heures hebdomadaires. Voici un petit retour sur comment ça se passe chez nous.

![Pieds dépassant d’un hamac](/img/hamac.jpg)

## Le cadre initial

Aux débuts de Codeurs en Liberté, nous étions très prudents. Nous avions estimé qu’avec un chiffre d’affaires de 2 000 € par mois, après avoir retiré 5 % de frais de fonctionnement et 5 % de réserve on peut financer un
[salaire légèrement au dessus du smic](https://mon-entreprise.fr/simulateurs/salaire-brut-net?cout-embauche=1900%E2%82%AC%2Fmois&utm_source=sharing).

Si on travaillait plus que ce minimum exigé, alors on avait droit à une prime de performance.

L’objectif était double : ne pas encourager à travailler trop tout en se protégeant d’un trou de trésorerie pour payer un salaire élevé.

## Les alternatives envisagées

Notre cadre est finalement très égoïste : si on travaille plus on gagne plus et il n’y a aucune ambition de mise en commun. Nous avons donc discuté, puis écarté d’autres modèles de répartition.

### Le salaire égal

Cette approche tentante par sa simplicité, mais nous gêne pour deux raisons :

* inconsciemment, on se dit qu’on doit fournir un travail à peu près équivalent à nos camarades. Cela peut entrainer une augmentation du temps de travail par syndrôme de l’imposteur ;
* nos besoins ne sont pas les mêmes (famille, achat immobilier, dépendance à la voiture…) ; acter un salaire égal revient à nier cette variété de vie.


### Le salaire au besoin

Il s’agit probablement de l’approche la plus ambitieuse. La boulangerie [le pain des cairns](https://www.lepaindescairns.fr/le-salaire-au-besoin/) l’a expérimenté.
Au bout de 6 mois d’interrogation, une approche plus conventionnelle a été adopté [en mai](https://www.lepaindescairns.fr/ptit-coup-doeil-sur-les-douze-derniers-mois/).
Ce retour d’expérience est très enrichissant, et nous espérons qu’ils inspireront d’autres structures.

Nous ne nous sentons pas prêts pour expérimenter une telle approche. Le « besoin » étant d’autant plus difficile à définir avec les hauts salaires en informatique — bien supérieurs à ceux de la boulangerie — où, de fait, les besoins sont déjà tous satisfaits.

## Les évolutions

Nous souhaitons quand même encourager à travailler moins et faciliter la vie des revenus plus faibles. Voici ce qui a changé :

* la contribution au pot commun passe de 10% à 15% ;
* une contribution mensuelle fixe de 500 € est versée à toustes ;
* nous versons également une prime de naissance équivalente à deux mois de SMIC pour compenser la faiblesse de la sécu ;
* les contributions à du logiciel libre existant sont rémunérées ;
* au fil des années, nous avons également mis un peu d’argent de côté. Cela permet de mieux éponger les retard de paiement et de se mettre « en négatif » en cas de creux d’activité.

Concrètement, voici le changement pour la base de calcul de la prime pour deux cas : une première personne générant 2 000 € de chiffre d’affaires et une autre 10 000 par mois :

* avant : `2000 × 0.9 = 1800` et `10000 × 0.9 = 9000`
* après : `2000 × 0.85 + 500 = 2200` et `10000 × 0.85 + 500 = 9000`

Cela permet donc de rendre une baisse du temps de travail (ou travailler pour des causes moins rémunératrices) un peu moins difficile à la fin du mois.

### L’outillage

![Vielle pointeuse](/img/pointeuse.jpg)

Initialement, nous utilisions un tableur libreoffice sur un partage de document. Ne faites jamais ça chez vous. Les fins de mois, les modifications simultannées obligaient à dire « je modifie le fichier, personne n’y touche ».

Le changement des règles de calcul de la contribution au pot commun et les 500 € distribués chaque mois à tout le monde ont rendu le tableur compliqué.

Nous avons donc développé notre petit outil qui s’appelle [Ardoise](https://gitlab.com/CodeursEnLiberte/ardoise/) qui nous permet d’estimer notre prime potentielle, ou au contraire s’il était temps de chercher une nouvelle mission pour assurer le chiffre d’affaire que nous devons apporter.

### Les limites

Notre modèle a pour but de réduire l’isolement par rapport à être indépendant·e.
Cela fonctionne, en partie. Nous aimerions travailler plus souvent sur les même projets, pas seulement côte-à-côte sur le même bateau.

Il est difficile de trouver des projets en prestation à la bonne échelle pour répondre collectivement.
De plus notre modèle rend complexe de travailler sur un produit qui serait le nôtre.

Cependant nous pensons pouvoir dépasser ceaà. À suivre !

## Bilan

Notre modèle était initialement très prudent et peu ambitieux.

Le temps passant, et après des toutes petites adaptations, nous en sommes plutôt satisfaits et y trouvons pas mal d’avantages.

Nous pensons que notre modèle encourage à prendre son temps. Dans les faits, tout le monde est au plus à 80%, avec des congés assez généreux. Certaines personnes facturent même moins de 100 jours par an.

En particulier, toute réduction d’activité d’une personne n’aura pas d’impact sur les chiffres et ne mettra pas le collectif en danger.

Réciproquement, une grande activité est possible et alimentera d’autant plus notre pot commun.
